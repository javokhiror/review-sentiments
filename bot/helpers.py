import json

import aiohttp
import requests

url = 'http://localhost:8000/'
endpoint = 'clients/'
url_path = f'{url}{endpoint}'


def get_organizations() -> list:
	r = requests.get(url=url_path)
	return r.json()


async def post_data(review_data: dict, url: str) -> None:
	async with aiohttp.ClientSession() as session:
		async with session.post(url=url, data={**review_data}) as response:
			data = await response.text()
			print("-> created")
			print(data)


async def user_exists(external_id) -> bool:
	async with aiohttp.ClientSession() as session:
		async with session.get(f'{url}bot_users/?external_id={external_id}') as response:
			r = await response.json()
			data = json.loads(r)
			return data
