import asyncio
import logging

import aiogram.utils.markdown as md
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ParseMode, KeyboardButton
from aiogram.utils import executor

from helpers import get_organizations, post_data, user_exists

API_TOKEN = '5825929432:AAGYijAAANI9cziw8dvuoeR-3gPCf4JgmXY'

logging.basicConfig(level=logging.INFO)
loop = asyncio.get_event_loop()

# Create Bot Instance
bot = Bot(token=API_TOKEN, loop=loop)

# For example use simple MemoryStorage for Dispatcher.
storage = MemoryStorage()

# It will process incoming updates: messages, edited messages, channel posts, edited channel posts,
# inline queries, chosen inline results, callback queries, shipping queries, pre-checkout queries.
dp = Dispatcher(bot, storage=storage)


# States
class Form(StatesGroup):
	name = State()  # Will be represented in storage as 'Form:name'
	age = State()  # Will be represented in storage as 'Form:age'
	gender = State()  # Will be represented in storage as 'Form:gender'
	organization = State()
	review = State()


@dp.message_handler(commands='start')
async def cmd_start(message: types.Message):
	"""
	Conversation's entry point
	"""
	# Set state
	await Form.name.set()

	await message.reply("Hi there! What's your name?")


# You can use state '*' if you need to handle all states
@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='cancel', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
	"""
	Allow user to cancel any action
	"""
	current_state = await state.get_state()
	if current_state is None:
		return

	logging.info('Cancelling state %r', current_state)
	# Cancel state and inform user about it
	await state.finish()
	# And remove keyboard (just in case)
	await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())


@dp.message_handler(state=Form.name)
async def process_name(message: types.Message, state: FSMContext):
	"""
	Process user name
	"""
	async with state.proxy() as data:
		data['name'] = message.text

	await Form.next()
	await message.reply("How old are you?")


# Check age. Age gotta be digit
@dp.message_handler(lambda message: not message.text.isdigit(), state=Form.age)
async def process_age_invalid(message: types.Message):
	"""
	If age is invalid
	"""
	return await message.reply("Age gotta be a number.\nHow old are you? (digits only)")


@dp.message_handler(lambda message: message.text.isdigit(), state=Form.age)
async def process_age(message: types.Message, state: FSMContext):
	# Update state and data
	async with state.proxy() as data:
		data['age'] = int(message.text)
	await Form.next()

	# Configure ReplyKeyboardMarkup
	markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
	markup.add("Male", "Female")
	markup.add("Other")

	await message.reply("What is your gender?", reply_markup=markup)


@dp.message_handler(lambda message: message.text not in ["Male", "Female", "Other"], state=Form.gender)
async def process_gender_invalid(message: types.Message):
	"""
	In this example gender has to be one of: Male, Female, Other.
	"""
	return await message.reply("Bad gender name. Choose your gender from the keyboard.")


@dp.message_handler(state=Form.gender)
async def process_gender(message: types.Message, state: FSMContext):
	# Update state and data
	async with state.proxy() as data:
		data['gender'] = message.text

		p_data = {
			"external_id": message.from_user.id,
			"name": data['name'],
			"age": data['age'],
			"gender": data['gender']
		}
		# send personal data to save it
		await post_data(review_data=p_data, url='http://localhost:8000/bot_users/')

	await Form.next()

	markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
	organizations = get_organizations()

	for item in organizations:
		markup.add(KeyboardButton(item['org_name']))

	await message.reply('Choose organization', reply_markup=markup)


@dp.message_handler(state=Form.organization)
async def process_organization(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		input_org = message.text
		organizations = get_organizations()
		for item in organizations:
			if item['org_name'] == input_org:
				data['organization'] = item['id']
				data['organization_name'] = item['org_name']
				break

	await Form.next()
	await message.reply("Write review")


@dp.message_handler(state=Form.review)
async def process_review(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['review'] = message.text

		markup = types.ReplyKeyboardRemove()

		# And send message
		await bot.send_message(
			message.chat.id,
			md.text(
				md.text('Hi! Nice to meet you,', md.bold(data['name'])),
				md.text('Age:', md.code(data['age'])),
				md.text('Gender:', data['gender']),
				md.text('Organization:', data['organization_name']),
				md.text('Your review:', data['review']),
				sep='\n',
			),
			reply_markup=markup,
			parse_mode=ParseMode.MARKDOWN,
		)
		await post_data({
			"organization": int(data['organization']),
			"bot_user": int(message.from_user.id),
			"content": data['review']
		}, url='http://localhost:8000/reviews/')

	# Finish conversation
	await Form.organization.set()


if __name__ == '__main__':
	executor.start_polling(dp, skip_updates=True)
