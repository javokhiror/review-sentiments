import csv
import urllib

from core.celery import app

import numpy as np
from scipy.special import softmax
from transformers import AutoModelForSequenceClassification, AutoTokenizer

COMPR_PUNCT = set('?!()')


# Preprocess text (username and link placeholders)
def preprocess(text):
	"""Compress multiple punctuation marks into single one

	Ex: 'Abc))))))))))' -> 'Abc)'
	"""
	new_text = []

	for t in text.split(" "):
		prev_ch = ''
		res = ''

		for ch in t:
			if prev_ch not in COMPR_PUNCT or ch not in COMPR_PUNCT:
				res += ch

			prev_ch = ch

		new_text.append(res)

	return " ".join(new_text)


task = 'sentiment'
MODEL = f"cardiffnlp/twitter-roberta-base-{task}"

tokenizer = AutoTokenizer.from_pretrained(MODEL)

# download label mapping
# 0	negative
# 1	neutral
# 2	positive
labels = []
mapping_link = f"https://raw.githubusercontent.com/cardiffnlp/tweeteval/main/datasets/{task}/mapping.txt"
with urllib.request.urlopen(mapping_link) as f:
	# make request to mapping_link to get labels
	html = f.read().decode('utf-8').split("\n")

	# convert to csv table form
	csvreader = csv.reader(html, delimiter='\t')

labels = [row[1] for row in csvreader if len(row) > 1]

# we get cardiffnlp/twitter-roberta-base-sentiment model from huggingface.co
model = AutoModelForSequenceClassification.from_pretrained(MODEL)


@app.task
def process_review(review_id, text):
	from metrics.models import Metrics

	text = preprocess(text)
	encoded_input = tokenizer(text, return_tensors='pt')
	output = model(**encoded_input)

	# creating matrix
	scores = output[0][0].detach().numpy()
	# The softmax function transforms each element of a collection by
	# computing the exponential of each element divided by the sum of the
	# exponentials of all the elements. That is, if `x` is a one-dimensional
	scores = softmax(scores)

	ranking = np.argsort(scores)

	# 2, 1, 0 -> 0, 1, 2
	ranking = ranking[::-1]
	tone_mapping = {}
	for i in range(scores.shape[0]):
		tone_mapping[labels[ranking[i]]] = scores[ranking[i]]

	Metrics.objects.create(
		review_id=review_id.id,
		positive=tone_mapping['positive'],
		neutral=tone_mapping['neutral'],
		negative=tone_mapping['negative']
	)
