from django.urls import path
from metrics import views

urlpatterns = [
	path('reviews/', views.ReviewList.as_view()),
	path('reviews/<int:pk>/', views.ReviewDetail.as_view()),
	path('bot_users/', views.BotUserList.as_view()),
	path('bot_users/<int:pk>/', views.BotUserDetail.as_view()),
]
