from django.db import models

from clients.models import Client

from metrics.tasks import process_review


class BotUser(models.Model):
	external_id = models.CharField(max_length=256)
	name = models.CharField(max_length=256, null=True)
	age = models.SmallIntegerField(null=True)
	gender = models.CharField(max_length=256, null=True)

	def __str__(self):
		return f'{self.external_id}'


class Review(models.Model):
	organization = models.ForeignKey(Client, on_delete=models.CASCADE)
	bot_user = models.ForeignKey(BotUser, on_delete=models.CASCADE, null=True)
	content = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def save(
			self, force_insert=False, force_update=False, using=None, update_fields=None
	):
		super(Review, self).save(
			force_insert=False, force_update=False, using=None, update_fields=None
		)
		process_review(text=self.content, review_id=self)

	def __str__(self):
		return f'{self.content}'


class Metrics(models.Model):
	review = models.ForeignKey(Review, on_delete=models.CASCADE)
	positive = models.FloatField(null=True)
	neutral = models.FloatField(null=True)
	negative = models.FloatField(null=True)

	def __str__(self):
		return f'{self.review}'

	@property
	def tone(self) -> str:
		if self.neutral > self.negative and self.neutral > self.positive:
			return 'Neutral'
		elif self.positive > self.neutral and self.positive > self.negative:
			return 'Positive'
		elif self.negative > self.positive and self.negative > self.neutral:
			return 'Negative'
