from rest_framework.serializers import ModelSerializer

from metrics.models import Review
from metrics.models import BotUser


class BotUserSerializer(ModelSerializer):
	class Meta:
		model = BotUser
		fields = ('external_id', 'name', 'age', 'gender')

	def create(self, validated_data):
		# Get or Create Bot user in order to avoid
		instance, _ = self.Meta.model.objects.get_or_create(**validated_data)
		return instance


class ReviewSerializer(ModelSerializer):
	class Meta:
		model = Review
		fields = ('organization', 'bot_user', 'content')
		extra_kwargs = {
			'bot_user': {'read_only': True}  # define the 'user' field as 'read-only'
		}
