from django.contrib import admin

from metrics.models import Review
from metrics.models import BotUser
from metrics.models import Metrics


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
	pass


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
	pass


@admin.register(Metrics)
class MetricsAdmin(admin.ModelAdmin):
	list_display = ['review_content', 'positive', 'neutral', 'negative', 'tone']

	# pass

	def review_content(self, obj):
		return obj.review.content
