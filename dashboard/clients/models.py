from django.db import models


class Client(models.Model):
	name = models.CharField(max_length=256)
	org_name = models.CharField(max_length=256)
	phone = models.IntegerField(null=True)
	email = models.EmailField(null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return f'{self.name} | {self.org_name}'

	class Meta:
		ordering = ['created_at']
