Prerequisites:
python3

1. We create virtual environment for our dependencies

python3 -m venv venv

2. Then we activate it from directory where we create venv

source venv/bin/activate

3. Install dependencies

pip3 install -r requirements.txt

4. Start Django:
    - create migrations
        • python3 dashboard/manage.py makemigrations
    - migrate
        • python3 dashboard/manage.py migrate
    - start Django project
        • python3 dashboard/manage.py runserver
    - to enter to django admin you need to create user
        • python dashboard/manage.py createsuperuser

5. Start Bot
    - start polling
        • python3 -m bot/fstate

6. To see result of model go to http://localhost:8000/admin/metrics/metrics/
